/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mediaplayer;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.FileChooser;
import static javax.swing.Spring.width;
import javafx.util.Duration;

/**
 *
 * @author Milan
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    private MediaPlayer mediaPlayer;
    
    @FXML
    private MediaView mediaView;
    
    private String filePath;
    
    @FXML
    private Slider sliderZvuk;
    
    @FXML
    private Slider sliderVreme;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Izaberite MP4/MP3 fajl", "*.mp4" , "*.mp3");
            fileChooser.getExtensionFilters().add(filter);//Filter koristimo za filtriranje ekstenzija u selection view
            File file = fileChooser.showOpenDialog(null);
            filePath = file.toURI().toString();//Putanja do medije/klipa
            
            if(filePath != null){
                Media media = new Media (filePath);
                mediaPlayer = new MediaPlayer(media);
                mediaView.setMediaPlayer(mediaPlayer);
                    DoubleProperty sirina = mediaView.fitWidthProperty();
                    DoubleProperty duzina = mediaView.fitHeightProperty();
                    
                    sirina.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
                    duzina.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));
                
                    sliderZvuk.setValue(mediaPlayer.getVolume() * 100);
                    sliderZvuk.valueProperty().addListener(new InvalidationListener() {
                    @Override
                    public void invalidated(Observable observable) {
                        mediaPlayer.setVolume(sliderZvuk.getValue()/ 100);
                    }
                });
                
                mediaPlayer.currentTimeProperty().addListener(new ChangeListener <Duration>() {
                    @Override
                    public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
                        sliderVreme.setValue(newValue.toSeconds());
                    }
                });
                sliderVreme.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        mediaPlayer.seek(Duration.seconds(sliderVreme.getValue()));
                    }
                });    
                    
                
                
                
                    mediaPlayer.play();}
    }
    
    @FXML
    private void pauzirajVideo(ActionEvent event){
        mediaPlayer.pause();
    }
    @FXML
    private void playVideo(ActionEvent event){
        mediaPlayer.play();
    }
    @FXML
    private void prekidVideo(ActionEvent event){
        mediaPlayer.stop();
    }
    @FXML
    private void brzoVideo(ActionEvent event){
       mediaPlayer.setRate(1.5); 
    }
    @FXML
    private void brzeVideo(ActionEvent event){
       mediaPlayer.setRate(2);  
    }
    @FXML
    private void normalnaVideo(ActionEvent event){
        mediaPlayer.setRate(1); 
    }
    @FXML
    private void sporoVideo(ActionEvent event){
        mediaPlayer.setRate(0.75); 
    }
    @FXML
    private void sporijeVideo(ActionEvent event){
        mediaPlayer.setRate(0.5); 
    }
    @FXML
    private void izlazVideo(ActionEvent event){
        System.exit(0); 
    }
    @FXML
    //private void fullScreen(ActionEvent event){
        //mediaPlayer.FullScreen();
    ///}
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
